# Feature MR Template

## Title
([Ticket#](feat):[your ticket] add claim damage and new claim pages)

## Summary
(Summarize the feature concisely)

## Relevant screenshots
(Paste any relevant screenshots if you have any)

## Testing instructions
(If you can, add necessary steps/pre-requisites to test the feature)
